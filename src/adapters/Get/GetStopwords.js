import axios from "axios";

export default async function GetStopwords(text, cb) {
  axios
    .get(`http://localhost:5000/getStopwords/${text}`)
    .then(res => {
      cb(res.data);
    })
    .catch(e => {
      console.log("Failed to get: " + e);
    });
}
