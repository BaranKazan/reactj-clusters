import axios from "axios";

export default async function RemoveStopwords(pattern, cb) {
  if (pattern) {
    axios
      .get(`http://localhost:5000/getStopwords/${pattern}`)
      .then(res => cb(res.data))
      .catch(e => console.log(`Could not fetch data: ${e}`));
  } else if (pattern === " ") {
    cb([]);
  } else {
    cb([]);
  }
}
