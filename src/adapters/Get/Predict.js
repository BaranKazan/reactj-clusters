import axios from "axios";

export default async function Predict(text, title, cb) {
  axios
    .get(
      `http://localhost:5000/predict/${encodeURIComponent(
        text.replace("/", " ")
      )}/${encodeURIComponent(title.replace("/", " "))}`
    )
    .then(res => {
      cb(res.data);
    })
    .catch(e => {
      console.log("Failed to get: " + e);
    });
}
