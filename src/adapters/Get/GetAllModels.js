import axios from "axios";

export default async function GetAllModels(cb) {
  axios
    .get(`http://localhost:5000/getAllModels`)
    .then(res => {
      cb(res.data);
    })
    .catch(e => {
      console.log("Failed to get: " + e);
    });
}
