import axios from "axios";

export default async function OpenModel(
  modelName,
  wordNr,
  callbackSuccess,
  callbackFailed
) {
  axios
    .get(`http://localhost:5000/openModel/${modelName}/${wordNr}`)
    .then(res => {
      callbackSuccess(res.data);
    })
    .catch(e => {
      console.log("Failed to get: " + e);
      callbackFailed();
    });
}
