import axios from "axios";

export default async function RemoveModel(text, cb) {
  axios
    .get(`http://localhost:5000/removeModel/${text}`)
    .then(res => {
      cb(res);
    })
    .catch(e => {
      console.log(e);
    });
}
