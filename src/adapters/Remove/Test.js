import axios from "axios";

export default async function Test(cb) {
  axios
    .get(`http://localhost:5000/test`)
    .then(res => {
      cb(res.data);
    })
    .catch(e => {
      console.log(e);
    });
}
