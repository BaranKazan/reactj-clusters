import axios from "axios";

export default async function RemoveStopwords(text, cb) {
  axios
    .get(`http://localhost:5000/removeStopword/${text}`)
    .then(res => {
      cb(res);
    })
    .catch(e => {
      console.log(e);
    });
}
