import axios from "axios";

export default async function CreateModel(clusterNr, minWords, name, alg, testset, cb) {
  axios
    .get(
      `http://localhost:5000/createModel/${clusterNr}/${minWords}/${name}/${alg}/${testset}`
    )
    .then(res => {
      cb(res.data);
    })
    .catch(e => {
      console.log(e);
    });
}
