import axios from "axios";

export default async function AddStopwords(
  text,
  cb,
) {
  axios
    .get(
      `http://localhost:5000/addStopword/${text}`
    )
    .catch(e => {
      console.log(e);
    });
}
