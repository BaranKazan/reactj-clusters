import React, { Component } from "react";
import ModelModal from "./ModelModal";
import StopwordModal from "./StopwordModal";
import TestModal from "./TestModal";
import GetAllModels from "../adapters/Get/GetAllModels";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import {
  Navbar,
  NavbarBrand,
  NavItem,
  Collapse,
  NavbarToggler,
  Nav,
  NavLink
} from "reactstrap";

export default class NavBar extends Component {
  state = {
    modelModal: false,
    stopwordsModal: false,
    addModelModal: false,
    testModal: false,
    navbar: false,
    fwords: [],
    models: []
  };

  toggleModelModal = () =>
    this.setState({ modelModal: !this.state.modelModal });
  toggleStopwordsModal = () =>
    this.setState({ stopwordsModal: !this.state.stopwordsModal });
  toggleTestModal = () =>
    this.setState({ testModal: !this.state.testModal });
  toggleAddModelModal = () =>
    this.setState({ addModelModal: !this.state.addModelModal });
  toggleNavbar = () => this.setState({ navbar: !this.state.navbar });

  render() {
    return (
      <div>
        <Navbar
          color="dark"
          dark
          className="navbar shadow-sm p-3 mb-5 navbar-dark bg-dark rounded"
          expand="lg"
          sticky={"top"}
        >
          <FontAwesomeIcon
            icon={faPaperPlane}
            color="white"
            style={{ marginRight: "1rem" }}
          />
          <NavbarBrand href="/" id="navbarTitle" className="mr-auto">
            IT - Support{" "}
          </NavbarBrand>
          <NavbarToggler onClick={this.toggleNavbar} />
          <Collapse isOpen={this.state.navbar} navbar>
            <Nav navbar>
              <NavItem>
                <NavLink href="#" onClick={this.getModels}>
                  Models
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#" onClick={this.toggleStopwordsModal}>
                  Stopwords
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#" onClick={this.toggleTestModal}>
                  Tests
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        <ModelModal
          isOpen={this.state.modelModal}
          toggle={this.toggleModelModal}
          toggle2={this.toggleAddModelModal}
          addModal={this.state.addModelModal}
          modelChange={fwords => this.props.onModelChange(fwords)}
          models={this.state.models}
        />
        <StopwordModal
          isOpen={this.state.stopwordsModal}
          toggle={this.toggleStopwordsModal}
        />
        <TestModal
          isOpen={this.state.testModal}
          toggle={this.toggleTestModal}
        />
      </div>
    );
  }
  update = models => {
    this.setState({
      models: models.map(s => {
        return s;
      })
    });
  };
  getModels = () => {
    GetAllModels(this.update);
    this.toggleModelModal();
  };
}
