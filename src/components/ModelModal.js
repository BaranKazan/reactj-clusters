import React, { Component } from "react";
import RemoveModel from "../adapters/Remove/RemoveModel";
import CreateMod from "../adapters/Add/CreateModel";
import OpenModel from "../adapters/Get/OpenModel";
import Loader from "react-loader-spinner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import TestModal from "./TestModal";
import {
  faFolderOpen,
  faShareSquare,
  faMinus,
  faFolderPlus
} from "@fortawesome/free-solid-svg-icons";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Input,
  Label,
  FormGroup,
  Form
} from "reactstrap";

export default class NavBar extends Component {
  state = {
    modelModal: false,
    stopwordsModal: false,
    addModelModal: false,
    navbar: false,
    fileName: "",
    clusterNr: 0,
    wordNr: 10,
    fWords: [],
    stopword: "",
    stopwords: [],
    spinner: false,
    ALG: 1,
    testSet: 1000
  };
  render() {
    if (this.state.spinner === true) {
      return (
        <div>
          <Loader type="ThreeDots" color="#2BAD60" height={150} width={150} />
        </div>
      );
    }
    return (
      <div>
        <div>
          <Modal isOpen={this.props.isOpen} toggle={this.props.toggle}>
            <ModalHeader toggle={this.props.toggle}>Model</ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label>Select Model</Label>
                <Input
                  defaultValue={"DEFAULT"}
                  type="select"
                  onChange={this.setModelName}
                >
                  <option value="DEFAULT" disabled hidden>
                    Choose Model
                  </option>
                  {this.props.models.map((s, i) => (
                    <option key={i}>{s}</option>
                  ))}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label>Input Amount of Words per Cluster</Label>
                <Input
                  type="number"
                  onChange={this.setModelWords}
                  required
                ></Input>
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button
                className="btn-success"
                onClick={this.openMod}
                title="Open"
              >
                <FontAwesomeIcon icon={faFolderOpen} />
              </Button>{" "}
              <Button
                className="btn-info"
                onClick={this.props.toggle2}
                title="New"
              >
                <FontAwesomeIcon icon={faShareSquare} />
              </Button>
              <Form onSubmit={this.removeMod}>
                <Button className="btn-danger" type="submit" title="Delete">
                  <FontAwesomeIcon icon={faMinus} />
                </Button>
              </Form>
            </ModalFooter>
          </Modal>
        </div>
        <div>
          <Modal isOpen={this.props.addModal} toggle={this.props.toggle2}>
            <ModalHeader toggle={this.props.toggle2}>Add Model</ModalHeader>
            <ModalBody>
              <FormGroup check>
                <Label check>
                  <Input
                    type="radio"
                    name="radio1"
                    checked={this.state.ALG === 1}
                    onChange={this.cr1}
                  />{" "}
                  KMEANS Algorithm
                </Label>
              </FormGroup>
              <FormGroup check>
                <Label check>
                  <Input
                    type="radio"
                    checked={this.state.ALG === 0}
                    onChange={this.cr2}
                  />{" "}
                  LDA Algorithm
                </Label>
              </FormGroup>
              <br />
              <FormGroup>
                <Label>Input Model Name</Label>
                <Input
                  type="text"
                  onChange={this.setModelName}
                  required
                ></Input>
              </FormGroup>
              <FormGroup>
                <Label>Input Model Clusters</Label>
                <Input
                  type="number"
                  onChange={this.setModelClusters}
                  required
                ></Input>
              </FormGroup>
              <FormGroup>
                <Label>Input Amount of Words per Cluster</Label>
                <Input
                  type="number"
                  onChange={this.setModelWords}
                  required
                ></Input>
              </FormGroup>
              <FormGroup>
                <Label>Input Test-Set size:</Label>
                <Input
                  type="number"
                  onChange={this.setTestset}
                  required
                ></Input>
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button
                className="btn-info"
                onClick={this.createMod}
                title="Create New Model"
              >
                <FontAwesomeIcon icon={faFolderPlus} />
              </Button>{" "}
            </ModalFooter>
          </Modal>
        </div>
        <TestModal clusterNr={this.state.clusterNr} />
      </div>
    );
  }
  cr1 = () => {
    this.setState({
      ALG: 1
    });
  };
  cr2 = () => {
    this.setState({
      ALG: 0
    });
  };
  removeMod = () => {
    RemoveModel(this.state.fileName);
  };

  setModelClusters = e => {
    this.setState({
      clusterNr: e.target.value
    });
  };
  setTestset = e => {
    this.setState({
      testSet: e.target.value
    });
  };

  setModelWords = e => {
    this.setState({
      wordNr: e.target.value
    });
  };
  setModelName = e => {
    this.setState({
      fileName: e.target.value
    });
  };
  updateFwordOpen = fWords => {
    this.setState({
      fWords: fWords,
      spinner: false
    });
    this.props.modelChange(fWords);
    this.props.toggle();
  };

  updateFwordsCreate = fWords => {
    this.setState({
      fWords: fWords,
      spinner: false
    });
    this.props.modelChange(fWords);
    this.props.toggle();
    this.props.toggle2();
  };
  openMod = () => {
    this.setState({
      spinner: true
    });
    let callbackFailed = () => {
      this.setState({
        spinner: false
      });
      this.props.toggle();
    };
    OpenModel(
      this.state.fileName,
      this.state.wordNr,
      this.updateFwordOpen,
      callbackFailed
    );
  };
  createMod = () => {
    CreateMod(
      this.state.clusterNr,
      this.state.wordNr,
      this.state.fileName,
      this.state.ALG,
      this.state.testSet,
      this.updateFwordsCreate
    );
    this.setState({
      spinner: true
    });
  };
}
