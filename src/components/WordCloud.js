import React, { Component } from "react";
import ReactWordcloud from "react-wordcloud";
import { Col, Row } from "reactstrap";

export default class WordCloud extends Component {
  render() {
    return (
      <div style={{ height: 400, width: 800 }}>
        <Row className="justify-content-md-center justify-content-xs-center justify-content-lg-center">
          <Col sm={12} xs={6} md={6} lg={6}>
            <ReactWordcloud options={options} words={this.props.words} />
          </Col>
        </Row>
      </div>
    );
  }
}

const options = {
  colors: ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b"],
  enableTooltip: true,
  deterministic: false,
  fontFamily: "impact",
  fontSizes: [20, 60],
  fontStyle: "normal",
  fontWeight: "normal",
  padding: 1,
  rotations: 1,
  rotationAngles: [0, 0],
  scale: "sqrt",
  spiral: "archimedean",
  transitionDuration: 1000
};
