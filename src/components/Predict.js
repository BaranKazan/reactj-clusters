import React, { Component } from "react";
import Predictor from "../adapters/Get/Predict";
import { Input, Label, Button, Col, FormGroup, Row } from "reactstrap";

export default class Predict extends Component {
  state = {
    title: "",
    text: "",
    cluster: []
  };
  render() {
    return (
      <div>
        <Row className="justify-content-md-center">
          <Col sm={12} xs={12} md={12} lg={6}>
            <FormGroup>
              <Label>Input Ticket Title:</Label>
              <Input
                type="text"
                placeholder="Ticket Title"
                onChange={this.predictTitle}
              ></Input>
            </FormGroup>
          </Col>
        </Row>
        <Row className="justify-content-md-center">
          <Col sm={12} xs={12} md={12} lg={6}>
            <FormGroup>
              <Label>Input Ticket Details:</Label>
              <Input
                type="textarea"
                placeholder="Description of issue"
                onChange={this.predictText}
              />
            </FormGroup>
          </Col>
        </Row>
        <div className="col text-center">
          <Row className="justify-content-md-center">
            <Col sm={12} xs={12} md={12} lg={6}>
              <Button onClick={this.predict}>Predict</Button>
            </Col>
          </Row>
          <Row className="justify-content-md-center">
            <Col sm={12} xs={12} md={12} lg={6}>
              <Label>{this.state.cluster}</Label>
            </Col>
          </Row>
        </div>
      </div>
    );
  }

  update = cluster => {
    this.setState({
      cluster: cluster
    });
  };

  predict = () => {
    Predictor(this.state.text, this.state.title, this.update);
  };

  predictText = e => {
    this.setState({ text: e.target.value });
  };
  predictTitle = e => {
    this.setState({ title: e.target.value });
  };
}
