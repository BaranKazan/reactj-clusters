import React, { Component } from "react";
import CheckListItem from "./CheckListItem";

export default class CheckList extends Component {
  render() {
    return this.props.words.map((word, index) => (
      <div key={index}>
        <CheckListItem
          word={word}
          onRemove={word => this.props.onRemove(word)}
        />
        <hr></hr>
      </div>
    ));
  }
}
