import React, { Component } from "react";
import PropTypes from "prop-types";
import WordCloud from "./WordCloud";
import { Label, Col, Row } from "reactstrap";

export default class Clusters extends Component {
  render() {
    return (
      <div>
        {this.props.clusters.map((cluster, index) => {
          return (
            <Row className="justify-content-md-center justify-content-xs-center justify-content-lg-center" key={index}>
              <Col sm={12} xs={12} md={12} lg={6}>
                <Label style={{ paddingTop: "10px", paddingBottom: "10px" }}>
                  Kö: {index}
                </Label>
                <WordCloud
                  words={cluster.map(word => {
                    return {
                      text: word[0],
                      value: word[1]
                    };
                  })}
                ></WordCloud>
              </Col>
            </Row>
          );
        })}
      </div>
    );
  }
}

Clusters.propTypes = {
  clusters: PropTypes.array.isRequired
};
