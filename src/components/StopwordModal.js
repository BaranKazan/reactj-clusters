import React, { Component } from "react";
import SearchStopwords from "../adapters/Get/SearchStopwords";
import AddStopword from "../adapters/Add/AddStopwords";
import RemoveStopword from "../adapters/Remove/RemoveStopwords";
import CheckList from "./CheckList";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSave } from "@fortawesome/free-solid-svg-icons";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Input,
  Label,
  FormGroup,
  Alert
} from "reactstrap";

export default class StopwordModal extends Component {
  state = {
    stopword: "",
    stopwords: [],
    pattern: "",
    alerVisible: false
  };
  render() {
    return (
      <div>
        <Modal isOpen={this.props.isOpen} toggle={this.toggleModelModal}>
          <ModalHeader toggle={this.toggleModelModal}>Stopwords</ModalHeader>
          <ModalBody>
            <Alert color="danger" isOpen={this.state.alerVisible} toggle={this.toggleAlertMessage}>
              Please input the stopword.
            </Alert>
            <FormGroup>
              <Label>Search Stopwords</Label>
              <Input
                onChange={e =>
                  SearchStopwords(
                    e.target.value,
                    stopwords => this.setState({ stopwords }),
                    this.setState({ pattern: e.target.value })
                  )
                }
              ></Input>
            </FormGroup>
            <FormGroup>
              <Label>Add Stopword</Label>
              <Input onChange={this.setStopword}></Input>
            </FormGroup>

            <FormGroup>
              <Label>Found stopwords:</Label>
              <CheckList
                words={this.state.stopwords}
                onRemove={word => this.removeStopword(word)}
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-success" onClick={this.addStopword} title="Add Stopword">
              <FontAwesomeIcon icon={faSave} />
            </Button>{" "}
          </ModalFooter>
          <ToastContainer
            autoClose={5000}
            containerId={"cc"}
            position={toast.POSITION.BOTTOM_CENTER}
          />
        </Modal>
      </div>
    );
  }

  toggleModelModal = () => {
    this.props.toggle()
    this.setState({stopwords: []})
  }

  setStopword = e => {
    this.setState({
      stopword: e.target.value
    });
  };

  notifyAdd = () => {
    toast("Added Stopword: " + this.state.stopword, {
      containerId: "cc"
    });
  };

  notifyRemove = e => {
    toast("Removed Stopword: " + e, {
      containerId: "cc"
    });
  };
  update = () => {
    SearchStopwords(this.state.pattern, stopwords =>
      this.setState({ stopwords })
    );
  };
  removeStopword = word => {
    RemoveStopword(word, () => {});
    this.notifyRemove(word);
    this.update();
  };

  addStopword = () => {
    if(this.state.stopword.trim() === ''){
      this.toggleAlertMessage()
    } else{
    AddStopword(this.state.stopword);
    this.notifyAdd();
  }
  };

  toggleAlertMessage = () => {
    this.setState({alerVisible: !this.state.alerVisible})
  }
}
