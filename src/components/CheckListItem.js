import React, { Component } from "react";

export default class CheckListItem extends Component {
  render() {
    return (
      <div style={itemStyle}>
        <p>
          {this.props.word}
          <button
            onClick={e => this.props.onRemove(this.props.word)}
            style={btnStyle}
          >
            x
          </button>
        </p>
      </div>
    );
  }
}

const itemStyle = {
  padding: "0px 0px 0px 30px",
  borderBotton: "1px #ccc dotted",
  textDecoration: "none"
};

const btnStyle = {
  display: "inline-block",
  cursor: "pointer",
  color: "#050505",
  fontFamily: "Arial",
  fontSize: "14px",
  fontWeight: "bold",
  padding: "0px 4px",
  textDecoration: "none",
  float: "right"
};
