import React, { Component } from "react";
import Test from "../adapters/Remove/Test";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  FormGroup,
  Label
} from "reactstrap";

export default class TestModal extends Component {
  state = {
    result: []
  };
  render() {
    return (
      <div>
        <Modal isOpen={this.props.isOpen} toggle={this.toggleModelModal}>
          <ModalHeader toggle={this.toggleModelModal}>Tests</ModalHeader>
          <ModalBody>
            <FormGroup>
              <Button
                className="btn-success"
                onClick={this.update}
                title="Run Test"
              >
                Run Test
              </Button>{" "}
            </FormGroup>
            <FormGroup>
              {this.state.result.map((list, index) => {
                return (
                  <div>
                    <Label>Kö: {index}</Label>
                    <br />
                    {this.onlyUnique(list).map(uniqueValue => {
                      return (
                        <div>
                          <Label>
                            The DB value {uniqueValue} has:{" "}
                            {
                              list.filter(
                                x => parseInt(x) === parseInt(uniqueValue)
                              ).length
                            }
                          </Label>
                          <br />
                        </div>
                      );
                    })}
                  </div>
                );
              })}
            </FormGroup>
          </ModalBody>
          <ModalFooter></ModalFooter>
        </Modal>
      </div>
    );
  }

  toggleModelModal = () => {
    this.props.toggle();
    this.setState({ result: [] });
  };

  onlyUnique = list => {
    return Array.from(new Set(list));
  };

  calcRes = result => {
    var x = [];
    var z = this.props.clusterNr;
    this.setState({
      result: result.map(s => {
        for (var i = 0; i < z; i++) {}
        return x;
      })
    });
  };

  update = () => {
    Test(data => {
      this.setState({ result: data });
    });
  };
}
