import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import NavBar from "./components/NavBar";
import Clusters from "./components/Clusters";
import Predict from "./components/Predict";

export default class App extends Component {
  state = {
    clusterWords: []
  };

  render() {
    return (
      <div className="App wrapper">
        <NavBar
          onModelChange={clusterWords => this.setState({ clusterWords })}
        />
        <Predict />
        
        <Clusters clusters={this.state.clusterWords} />
        
      </div>
    );
  }
}
